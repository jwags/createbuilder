# Class Name
OperationResourceBusinessLogic
# Dependencies
IRigIsLogger rigIsLogger, IOperationResourceDataService operationResourceDataService, IApplicationUserInfo applicationUserInfo, IBasisTypeDataService basisTypeDataService, IChargeTypeDataService chargeTypeDataService, IOperationDataService operationDataService, ISystemServiceClient systemServiceClient, IItemServiceClient itemServiceClient, IMfgResourceServiceClient mfgResourceServiceClient, IWorkOrderDataService workOrderDataService, IInventoryOrgServiceClient inventoryOrgServiceClient, IMessageToReprocessDataService messageToReprocessDataService, IMessageSender messageSender, IPersonServiceClient personServiceClient, IApplicationConfiguration applicationConfiguration, IPassagewayServiceClient passagewayServiceClient
