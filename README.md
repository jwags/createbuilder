# Create Builder Class

This program will automate the creation of a Builder Class. A builder class is a class used to manage injections when testing a class.

Note: Built in Python 3.9.0 and currently only build Builder Classes for C#.

Demo:

![enter image description here](https://media.giphy.com/media/kGdRvjcglvuPbXz44Z/giphy.gif)

