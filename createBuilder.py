# Built By Jordan Wagner
# On 12-6-2019
# Version 0.0.1
# Description:
# This script automates the creation of a builder class. A builder class is a class used to manage injections when testing a class.
# Note: Currently built only for C#
#
# Input: Class Name
#   Enter the name of the class the builder is for.
#   Example: PersonBusinessLogic
# Input: Default Dependencies (optional)
#   Enter a comma seperated list of default dependencies and their interface.
#   Example: INullLogger nullLogger, IDateTimeNow dateTimeNow
# Input: Dependencies
#   Enter a comma seperated list of dependencies and their interface.
#   Example: IMyLogger logger, IDateTimeNow dateTimeNow, IPersonDataService personDataService
# 
# Output: The Builder Class
# Example:
# public class PersonBusinessLogicBuilder
# {
#   private IMyLogger _Logger { get; set; }
#   private IDateTimeNow _DateTimeNow { get; set; }
#   private IPersonDataService _PersonDataService { get; set; }
# 
#   public PersonBusinessLogic Build()
#   {
#     return new PersonBusinessLogic(this._Logger,
#                                    this._DateTimeNow,
#                                    this._PersonDataService);
#   }
# 
#   public PersonBusinessLogicBuilder WithDefaults()
#   {
#     this._NullLogger = new NullLogger();
#     this._DateTimeNow = new DateTimeNow();
#     return this;
#   }
# 
#   public PersonBusinessLogicBuilder With(IMyLogger dependency)
#   {
#     this._Logger = dependency;
#     return this;
#   }
# 
#   public PersonBusinessLogicBuilder With(IDateTimeNow dependency)
#   {
#     this._DateTimeNow = dependency;
#     return this;
#   }
# 
#   public PersonBusinessLogicBuilder With(IPersonDataService dependency)
#   {
#     this._PersonDataService = dependency;
#     return this;
#   }
# }
# 
# 
# Use like so:
# var sut = new PersonBusinessLogicBuilder().WithDefaults().With(stubPersonDataService).Build()

from string import Template
from os import system, name 

# Clear terminal
def clear(): 
    # for windows 
    if name == 'nt': 
        _ = system('cls') 
  
    # for mac and linux(here, os.name is 'posix') 
    else: 
        _ = system('clear')
clear()


# Create the variables in the beginning of the class
def createDependencyVariables(dependencies):
    classString = ''
    for dependency in dependencies:
        dependency[1] = dependency[1][0].capitalize() + dependency[1][1:]
        classString += '\t\tprivate ' + dependency[0] + ' _' + dependency[1] + ' { get; set; }\r\n'
    return classString


# Class name
className = ''
builderClassName = ''
while len(builderClassName) == 0:
    usrInput = input('Enter the class name the builder is for: ').strip()
    if usrInput == '':
        print('Invalid input')
    else:
        builderClassName = usrInput.strip()
        if len(builderClassName) == 0:
            print('Invalid input')
builderClassName = builderClassName if builderClassName.endswith('Builder') else builderClassName + 'Builder'
builderClassName = builderClassName[0].capitalize() + builderClassName[1:]
className = builderClassName[:-len('Builder')]


classString = '\tpublic class ' + builderClassName + '\r\n\t{\r\n'


# Default Dependencies (optional)
defaultDependencies = []
print('Enter any default dependencies as comma seperated list (Optional. Press enter to continue. Usually like a logger or Date class.)')
print('Example IMyFirstClass myFirstClass, IMySecondClass mySecondClass...')
while len(defaultDependencies) == 0:
    usrInput = input().strip()
    if usrInput == '':
        defaultDependencies = [['-99']]
    else:
        try:
            defaultDependencies = list(map(lambda x: x.strip().split(' '), usrInput.split(',')))
            if any(len(dependency) != 2 for dependency in defaultDependencies):
                print('Invalid input')
                defaultDependencies = []
        except:
            defaultDependencies = []
        
if defaultDependencies[0][0] == '-99':
    defaultDependencies = []


# Dependencies
dependencies = []
print('Enter dependencies as comma seperated list')
print('Example IMyFirstClass myFirstClass, IMySecondClass mySecondClass...')
while len(dependencies) == 0:
    usrInput = input().strip()
    if usrInput == '':
        print('Invalid input')
    else:
        try:
            dependencies = list(map(lambda x: x.strip().split(' '), usrInput.split(',')))
            if len(dependencies) == 0:
                print('Invalid input')
            if any(len(dependency) != 2 for dependency in dependencies):
                print('Invalid input')
                dependencies = []
        except:
            dependencies = []


# Create the Build Method
classString += createDependencyVariables(dependencies)
classString += Template(
                '\r\n'
                '\t\tpublic $className Build()\r\n'
                '\t\t{\r\n'
                '\t\t\treturn new $className('
                ).safe_substitute(className=className)

for dependency in dependencies:
    dependency[1] = dependency[1][0].capitalize() + dependency[1][1:]
    classString += Template('this._$dependencyName,\r\n\t\t\t').safe_substitute(dependencyName=dependency[1])
    classString += ' ' * len('return new ' + className + '(')

classString = classString.rstrip()

if classString.endswith(','):
    classString = classString[:-1]
classString += ');\r\n\t\t}\r\n\r\n'


# Create the WithDefaults Method for the default dependencies
if len(defaultDependencies) > 0:
    classString += Template(
        '\t\tpublic $builderClassName WithDefaults()\r\n'
        '\t\t{\r\n'
        ).safe_substitute(builderClassName=builderClassName)
    for dependency in defaultDependencies:
        dependency[1] = dependency[1][0].capitalize() + dependency[1][1:]
        dpendencyWithoutTheI = dependency[0] if dependency[0][0] != 'I' else dependency[0][1:]
        classString += Template('\t\t\tthis._$dependencyName = new $dpendencyWithoutTheI();\r\n').safe_substitute(dependencyName=dependency[1], dpendencyWithoutTheI=dpendencyWithoutTheI)
    classString += '\t\t\treturn this;\r\n\t\t}\r\n\r\n'



# Create the With methods for the dependencies
for dependency in dependencies:
    dependency[1] = dependency[1][0].capitalize() + dependency[1][1:]
    classString += Template(
        '\t\tpublic $builderClassName With($dependencyInterface dependency)\r\n'
        '\t\t{\r\n'
        '\t\t\tthis._$dependencyName = dependency;\r\n'
        '\t\t\treturn this;\r\n'
        '\t\t}\r\n\r\n'
        ).safe_substitute(builderClassName=builderClassName, dependencyInterface=dependency[0], dependencyName=dependency[1])
classString = classString.rstrip()
classString += '\r\n\t}'


# Print the class
print('\r\n\r\n\r\n\r\n\r\n\r\n')
print('Your Builder Class is:')
print(classString)
