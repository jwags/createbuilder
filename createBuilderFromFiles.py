# Built By Jordan Wagner
# On 12-6-2019
# Version 0.0.1
# Description:
# This script automates the creation of a builder class. A builder class is a class used to manage injections when testing a class.
# Note: Currently built only for C#
#
# Input: Class Name
#   Enter the name of the class the builder is for.
#   Example: PersonBusinessLogic
# Input: Default Dependencies (optional)
#   Enter a comma seperated list of default dependencies and their interface.
#   Example: INullLogger nullLogger, IDateTimeNow dateTimeNow
# Input: Dependencies
#   Enter a comma seperated list of dependencies and their interface.
#   Example: IMyLogger logger, IDateTimeNow dateTimeNow, IPersonDataService personDataService
# 
# Output: The Builder Class
# Example:
# public class PersonBusinessLogicBuilder
# {
#   private IMyLogger _Logger { get; set; }
#   private IDateTimeNow _DateTimeNow { get; set; }
#   private IPersonDataService _PersonDataService { get; set; }
# 
#   public PersonBusinessLogic Build()
#   {
#     return new PersonBusinessLogic(this._Logger,
#                                    this._DateTimeNow,
#                                    this._PersonDataService);
#   }
# 
#   public PersonBusinessLogicBuilder WithDefaults()
#   {
#     this._NullLogger = new NullLogger();
#     this._DateTimeNow = new DateTimeNow();
#     return this;
#   }
# 
#   public PersonBusinessLogicBuilder With(IMyLogger dependency)
#   {
#     this._Logger = dependency;
#     return this;
#   }
# 
#   public PersonBusinessLogicBuilder With(IDateTimeNow dependency)
#   {
#     this._DateTimeNow = dependency;
#     return this;
#   }
# 
#   public PersonBusinessLogicBuilder With(IPersonDataService dependency)
#   {
#     this._PersonDataService = dependency;
#     return this;
#   }
# }
# 
# 
# Use like so:
# var sut = new PersonBusinessLogicBuilder().WithDefaults().With(stubPersonDataService).Build()

from string import Template

# Create the variables in the beginning of the class
def createDependencyVariables(dependencies):
    classString = ''
    for dependency in dependencies:
        dependency[1] = dependency[1][0].capitalize() + dependency[1][1:]
        classString += '\t\tprivate Mock<' + dependency[0] + '> _' + dependency[1] + ' { get; set; }\n'
    return classString

builderInfoFile = open("builderInfo.txt", "r")
lines = builderInfoFile.readlines()

# Class name
className = ''
builderClassName = lines[1].strip()
builderClassName = builderClassName if builderClassName.endswith('Builder') else builderClassName + 'Builder'
builderClassName = builderClassName[0].capitalize() + builderClassName[1:]
className = builderClassName[:-len('Builder')]


classString = '\tpublic class ' + builderClassName + '\n\t{\n'




# Dependencies
dependencies = []
while len(dependencies) == 0:
    usrInput = lines[3].strip()
    if usrInput == '':
        print('Invalid input')
    else:
        try:
            dependencies = list(map(lambda x: x.strip().split(' '), usrInput.split(',')))
            if len(dependencies) == 0:
                print('Invalid input')
            if any(len(dependency) != 2 for dependency in dependencies):
                print('Invalid input')
                dependencies = []
        except:
            dependencies = []


#Create constructor
classString += '\t\tpublic ' + builderClassName + '()\n\t\t{\n'
for dependency in dependencies:
    dependency[1] = dependency[1][0].capitalize() + dependency[1][1:]
    classString += Template('\t\t\tthis._$dependencyName = new Mock<I$dependencyName>(MockBehavior.Loose);\n').safe_substitute(dependencyName=dependency[1])
classString += '\t\t}\n\n'


# Create the Build Method
classString += createDependencyVariables(dependencies)
classString += Template(
                '\n'
                '\t\tpublic $className Build()\n'
                '\t\t{\n'
                '\t\t\treturn new $className('
                ).safe_substitute(className=className)

for dependency in dependencies:
    dependency[1] = dependency[1][0].capitalize() + dependency[1][1:]
    classString += Template('this._$dependencyName.Object,\n\t\t\t').safe_substitute(dependencyName=dependency[1])
    classString += ' ' * len('return new ' + className + '(')

classString = classString.rstrip()

if classString.endswith(','):
    classString = classString[:-1]
classString += ');\n\t\t}\n\n'



# Create the With methods for the dependencies
for dependency in dependencies:
    dependency[1] = dependency[1][0].capitalize() + dependency[1][1:]
    classString += Template(
        '\t\tpublic $builderClassName With(Mock<$dependencyInterface> dependency)\n'
        '\t\t{\n'
        '\t\t\tthis._$dependencyName = dependency;\n'
        '\t\t\treturn this;\n'
        '\t\t}\n\n'
        ).safe_substitute(builderClassName=builderClassName, dependencyInterface=dependency[0], dependencyName=dependency[1])
classString = classString.rstrip()
classString += '\n\t}'


# Print the class
with open('builderResult.txt', 'w') as f:
    f.write(classString)

print('Your Builder Class is saved in builderResults.txt')
